## A simple implementation of the DSA algorithm
**DO NOT USE FOR REAL SIGNATURE**
___
### Build
Use Cmake (ver. 3.15 and higher) and [GNU MP library](https://gmplib.org/) to build:
```
$ mkdir build
$ cd build
$ cmake -DGNU_MP_LIB=<path to gmp> ..
$ cmake --build .
```
___
### Usage
```
Usage simple_dsa:
    --help,-h                                       Help message
    --generate,-g   <filename>                      Generate key pair files
    --sign,-s       <privkey> <message>             Sign message
    --validate,-v   <pubkey> <sign> <message>       Validate signature
```
Example:
```
$ ./simple_dsa -g my_key
Keys my_key_sdsa.private and my_key_sdsa.public are successfully generated
$ ./simple_dsa -s ./my_key_sdsa.private "hello world"
SIGNATURE: "982464A12EB008B2934C0B39982D56E3B218C4DB096D7F12FBCC9AFC23D2325C,B15C2145FC28C12ED3CE2783BC943C57014FEE5EFDA682B320A4CD7EE49BB5FF"
$ ./simple_dsa -v ./my_key_sdsa.public "982464A12EB008B2934C0B39982D56E3B218C4DB096D7F12FBCC9AFC23D2325C,B15C2145FC28C12ED3CE2783BC943C57014FEE5EFDA682B320A4CD7EE49BB5FF" "hello world"
SIGNATURE IS VALID
```

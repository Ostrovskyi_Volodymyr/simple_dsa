#include <ctype.h>
#include <getopt.h>
#include <stdlib.h>
#include <string.h>

// clang-format off
#include <stdio.h>
#include <gmp.h>
// clang-format on

#include "dsa.h"

// clang-format off
#define PUBKEY_FILE_POSTFIX     "_sdsa.public"
#define PRIVKEY_FILE_POSTFIX    "_sdsa.private"
// clang-format on

typedef struct opt {
    char type;
    const char *keyfile;
    const char *sign;
    const char *message;
} opt_t;

// clang-format off
static struct option long_options[] = {
    { "help", no_argument, NULL, 'h'},
    { "generate", required_argument, NULL, 'g'},
    { "sign", required_argument, NULL, 's'},
    { "validate", required_argument, NULL, 'v'},
    { 0, 0, 0, 0}
};
// clang-format on

static const char *optstring = "hg:s:v:";

static void print_usage(const char *progname) {
    printf("Usage %s:\n"
           "\t--help,-h\t\t\t\t\tHelp message\n"
           "\t--generate,-g\t<filename>\t\t\tGenerate key pair files\n"
           "\t--sign,-s\t<privkey> <message>\t\tSign message\n"
           "\t--validate,-v\t<pubkey> <sign> <message>\tValidate signature\n",
           progname);
}

static void key2file(const char *filename, mpz_t p, mpz_t q, mpz_t g, mpz_t key) {
    FILE *f = NULL;

    f = fopen(filename, "w");
    gmp_fprintf(f, "%ZX,%ZX,%ZX,%ZX\n", p, q, g, key);
    fclose(f);
}

static void generate_keys(const char *filename) {
    dsa_domain_t dsa;
    dsa_priv_key_t privkey;
    dsa_pub_key_t pubkey;
    char *keyfile = NULL;

    dsa_domain_init(&dsa);
    dsa_priv_key_init(&privkey);
    dsa_pub_key_init(&pubkey);

    dsa_domain_gen_params(&dsa);
    dsa_gen_keys(&dsa, &privkey, &pubkey);

    keyfile = malloc(strlen(filename) + sizeof(PRIVKEY_FILE_POSTFIX));
    sprintf(keyfile, "%s%s", filename, PRIVKEY_FILE_POSTFIX);
    key2file(keyfile, privkey.p, privkey.q, privkey.g, privkey.key);

    keyfile = malloc(strlen(filename) + sizeof(PUBKEY_FILE_POSTFIX));
    sprintf(keyfile, "%s%s", filename, PUBKEY_FILE_POSTFIX);
    key2file(keyfile, pubkey.p, pubkey.q, pubkey.g, pubkey.key);

    printf("Keys %s%s and %s%s are successfully generated\n", filename, PRIVKEY_FILE_POSTFIX,
           filename, PUBKEY_FILE_POSTFIX);

    dsa_domain_deinit(&dsa);
    dsa_priv_key_deinit(&privkey);
    dsa_pub_key_deinit(&pubkey);
    free(keyfile);
}

static int file2key(const char *filename, mpz_t *p, mpz_t *q, mpz_t *g, mpz_t *key) {
    FILE *f = NULL;

    f = fopen(filename, "r");
    if (!f)
        return 1;

    gmp_fscanf(f, "%ZX,%ZX,%ZX,%ZX\n", p, q, g, key);
    fclose(f);

    return 0;
}

static void sign_message(const char *keyfile, const char *msg) {
    dsa_priv_key_t privkey;
    dsa_signature_t sign;

    dsa_priv_key_init(&privkey);
    dsa_signature_init(&sign);

    if (file2key(keyfile, &privkey.p, &privkey.q, &privkey.g, &privkey.key)) {
        printf("ERROR: Cannot open key file\n");
        goto exit;
    }

    dsa_sign_message(&sign, &privkey, msg, strlen(msg));

    gmp_printf("SIGNATURE: \"%ZX,%ZX\"\n", sign.r, sign.s);

exit:
    dsa_priv_key_deinit(&privkey);
    dsa_signature_deinit(&sign);
}

static void validate_sign(const char *keyfile, const char *sign_str, const char *msg) {
    dsa_pub_key_t pubkey;
    dsa_signature_t sign;

    dsa_pub_key_init(&pubkey);
    dsa_signature_init(&sign);

    if (file2key(keyfile, &pubkey.p, &pubkey.q, &pubkey.g, &pubkey.key)) {
        printf("ERROR: Cannot open key file\n");
        goto exit;
    }

    if (gmp_sscanf(sign_str, "%ZX,%ZX", &sign.r, &sign.s) != 2) {
        printf("ERROR: Cannot scan signature\n");
        goto exit;
    }

    if (dsa_validate_sign(&sign, &pubkey, msg, strlen(msg))) {
        printf("SIGNATURE IS VALID\n");
    } else {
        printf("SIGNATURE IS INVALID\n");
    }

exit:
    dsa_pub_key_deinit(&pubkey);
    dsa_signature_deinit(&sign);
}

static void parse_args(int argc, char **argv, opt_t *ops) {
    int option_index = 0;
    int option = 0;

    while (1) {
        option = getopt_long(argc, argv, optstring, long_options, &option_index);
        if (option == -1)
            break;

        switch (option) {
            case 'g':
                ops->type = 'g';
                ops->keyfile = optarg;
                break;
            case 's':
                ops->type = 's';
                ops->keyfile = optarg;
                break;
            case 'v':
                ops->type = 'v';
                ops->keyfile = optarg;
                break;
            default:
                return;
        }
    }

    if ((ops->type == 's' && optind >= argc) || (ops->type == 'v' && optind + 1 >= argc)) {
        ops->type = 'h';
    } else {
        if (ops->type == 's') {
            ops->message = argv[optind];
        } else if (ops->type == 'v') {
            ops->sign = argv[optind];
            ops->message = argv[optind + 1];
        }
    }
}

int main(int argc, char **argv) {
    opt_t ops = {.type = 'h'};

    parse_args(argc, argv, &ops);

    switch (ops.type) {
        case 'g':
            generate_keys(ops.keyfile);
            break;
        case 's':
            sign_message(ops.keyfile, ops.message);
            break;
        case 'v':
            validate_sign(ops.keyfile, ops.sign, ops.message);
            break;
        case 'h':
        default:
            print_usage(argv[0]);
            exit(1);
    }

    return 0;
}

#include "dsa.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "sha-256.h"

// clang-format off
#define HASH_BITS   256
#define L_BITLEN    2048
#define N_BITLEN    HASH_BITS
// clang-format on

static void generate_PQ(mpz_t p, mpz_t q, size_t L, size_t N, size_t seedlen,
                        gmp_randstate_t randstate);
static void generate_G(mpz_t g, mpz_t p, mpz_t q, gmp_randstate_t randstate);
static void generate_keys(mpz_t x, mpz_t y, mpz_t p, mpz_t q, mpz_t g, gmp_randstate_t randstate);
static void generate_K(mpz_t k, mpz_t nk, mpz_t q, mpz_t g, gmp_randstate_t randstate);
static void generate_signature(mpz_t r, mpz_t s, mpz_t p, mpz_t q, mpz_t g, mpz_t x,
                               gmp_randstate_t randstate, const char *msg, size_t len);
static int validate_signature(mpz_t r, mpz_t s, mpz_t p, mpz_t q, mpz_t g, mpz_t y, const char *msg,
                              size_t len);

void dsa_domain_init(dsa_domain_t *dsad) {
    gmp_randinit_default(dsad->randstate);
    gmp_randseed_ui(dsad->randstate, time(NULL));
    mpz_inits(dsad->p, dsad->q, dsad->g, NULL);
}

void dsa_domain_deinit(dsa_domain_t *dsad) {
    gmp_randclear(dsad->randstate);
    mpz_clears(dsad->p, dsad->q, dsad->g, NULL);
}

void dsa_domain_gen_params(dsa_domain_t *dsad) {
    generate_PQ(dsad->p, dsad->q, L_BITLEN, N_BITLEN, N_BITLEN, dsad->randstate);
    generate_G(dsad->g, dsad->p, dsad->q, dsad->randstate);
}

void dsa_priv_key_init(dsa_priv_key_t *privkey) {
    mpz_inits(privkey->p, privkey->q, privkey->g, privkey->key, NULL);
}

void dsa_priv_key_deinit(dsa_priv_key_t *privkey) {
    mpz_clears(privkey->p, privkey->q, privkey->g, privkey->key, NULL);
}

void dsa_pub_key_init(dsa_pub_key_t *pubkey) {
    mpz_inits(pubkey->p, pubkey->q, pubkey->g, pubkey->key, NULL);
}

void dsa_pub_key_deinit(dsa_pub_key_t *pubkey) {
    mpz_clears(pubkey->p, pubkey->q, pubkey->g, pubkey->key, NULL);
}

void dsa_signature_init(dsa_signature_t *sign) {
    mpz_inits(sign->r, sign->s, NULL);
}

void dsa_signature_deinit(dsa_signature_t *sign) {
    mpz_clears(sign->r, sign->s, NULL);
}

void dsa_gen_keys(dsa_domain_t *dsad, dsa_priv_key_t *privkey, dsa_pub_key_t *pubkey) {
    generate_keys(privkey->key, pubkey->key, dsad->p, dsad->q, dsad->g, dsad->randstate);

    mpz_set(privkey->p, dsad->p);
    mpz_set(privkey->q, dsad->q);
    mpz_set(privkey->g, dsad->g);

    mpz_set(pubkey->p, dsad->p);
    mpz_set(pubkey->q, dsad->q);
    mpz_set(pubkey->g, dsad->g);
}

void dsa_sign_message(dsa_signature_t *sign, dsa_priv_key_t *privkey, const char *msg, size_t len) {
    gmp_randstate_t randstate;

    gmp_randinit_default(randstate);
    gmp_randseed_ui(randstate, time(NULL));

    generate_signature(sign->r, sign->s, privkey->p, privkey->q, privkey->g, privkey->key,
                       randstate, msg, len);

    gmp_randclear(randstate);
}

int dsa_validate_sign(dsa_signature_t *sign, dsa_pub_key_t *pubkey, const char *msg, size_t len) {
    return validate_signature(sign->r, sign->s, pubkey->p, pubkey->q, pubkey->g, pubkey->key, msg,
                              len);
}

static void get_hash(mpz_t hash, const char *msg, size_t len) {
    size_t i = 0;
    size_t block_count = 0;
    uint32_t **blocks = NULL;
    uint32_t h[HASH_BITS / 32] = {};
    char hash_str[HASH_BITS / 4 + 1] = {};

    // split message to blocks
    blocks = message2blocks(msg, len, &block_count);

    // get SHA256 hash of message
    sha_256(h, blocks, block_count);

    // convert array of hash values to hex-string
    for (i = 0; i < 8; ++i) {
        snprintf(hash_str + 8 * i, 65 - 8 * i, "%08x", h[i]);
    }

    // create 256-bit number from hash string
    mpz_init_set_str(hash, hash_str, 16);

    for (i = 0; i < block_count; ++i) free(blocks[i]);
    free(blocks);
}

/*
 * Generation of the Probable Primes P and Q using an Approved Hash Function
 * Input:
 *      1. L        The desired length of the prime P (in bits).
 *      2. N        The desired length of the prime Q (in bits).
 *      3. seedlen  The desired length of the domain parameter seed; seedlen shall be
 *                  equal to or greater than N.
 * Output:
 *      1. p,q      The generated primes p and q.
 */
static void generate_PQ(mpz_t p, mpz_t q, size_t L, size_t N, size_t seedlen,
                        gmp_randstate_t randstate) {
    const size_t outlen = HASH_BITS;
    size_t n = L / outlen - 1;
    size_t b = L - 1 - n * outlen;
    size_t offset = 0;
    mpz_t domain_seed, U, seed_hash, t, W, V, L2, Q2, C;
    char *ds_str = NULL;
    size_t i = 0, j = 0;

    if (seedlen < N)
        return;

    mpz_inits(domain_seed, U, seed_hash, t, W, V, L2, Q2, C, NULL);

    ds_str = malloc(HASH_BITS * 4 + 1);

    while (1) {
        // get arbitrary sequence of seedlen bits
        mpz_rrandomb(domain_seed, randstate, seedlen);
        // convert domain_seed number to hex-string
        gmp_sprintf(ds_str, "%ZX", domain_seed);
        // get hash of sequence of seedlen bits (hash of domain_seed)
        get_hash(seed_hash, ds_str, seedlen / 4);
        // U = seed_hash mod 2^(N-1)
        mpz_tdiv_r_2exp(U, seed_hash, N - 1);
        // t = U mod 2
        mpz_mod_ui(t, U, 2);
        // q = 2^(N-1)
        mpz_ui_pow_ui(q, 2, N - 1);
        // q += U
        mpz_add(q, q, U);
        // q += 1
        mpz_add_ui(q, q, 1);
        // q -= t (U mod 2)
        mpz_sub(q, q, t);

        // check if q is prime number
        if (mpz_probab_prime_p(q, 56) > 0)
            break;
    }

    offset = 1;
    // L2 = 2^(L-1)
    mpz_ui_pow_ui(L2, 2, L - 1);
    // Q2 = 2 * q
    mpz_mul_ui(Q2, q, 2);

    for (i = 0; i <= 4 * L - 1; ++i) {
        mpz_set(t, domain_seed);
        // t = domain_seed + offset
        mpz_add_ui(t, t, offset);
        mpz_set_ui(W, 0);

        for (j = 0; j <= n; ++j) {
            // V = domain_seed + offset + j
            mpz_add_ui(V, t, j);
            // V = V mod 2^seedlen
            mpz_tdiv_r_2exp(V, V, seedlen);
            gmp_sprintf(ds_str, "%ZX", V);
            // get hash of V
            get_hash(V, ds_str, strlen(ds_str));

            // if j == n then V = V mod 2^b
            if (j == n)
                mpz_tdiv_r_2exp(V, V, b);
            // V = V * 2^(j * outlen)
            mpz_mul_2exp(V, V, j * outlen);
            // W += V
            mpz_add(W, W, V);
        }

        // W += 2^(L-1)
        mpz_add(W, W, L2);
        // C = W mod (2 * q)
        mpz_mod(C, W, Q2);
        mpz_sub_ui(C, C, 1);
        // p = W - (C - 1)
        mpz_sub(p, W, C);

        // if p >= 2^(L-1)
        if (mpz_cmp(p, L2) >= 0) {
            // check if p is prime number
            if (mpz_probab_prime_p(p, 56) > 0)
                break;
        }

        offset += n + 1;
    }

    mpz_clears(domain_seed, U, seed_hash, t, W, V, L2, Q2, C, NULL);
    free(ds_str);
}

/*
 * Generation of the Generator g
 * Input:
 *      1. p,q      The generated primes.
 * Output:
 *      1. g        The requested value of g.
 */
static void generate_G(mpz_t g, mpz_t p, mpz_t q, gmp_randstate_t randstate) {
    mpz_t e, h;

    mpz_inits(e, h, NULL);

    // e = (p - 1) / q
    mpz_set(e, p);
    mpz_sub_ui(e, e, 1);
    mpz_fdiv_q(e, e, q);

    while (1) {
        mpz_set(h, p);
        mpz_sub_ui(h, h, 2);
        // randomize h in range (1, p -1)
        mpz_urandomm(h, randstate, h);
        mpz_add_ui(h, h, 2);
        // g = h^e mod p
        mpz_powm(g, h, e, p);
        if (mpz_cmp_ui(g, 1) != 0)
            break;
    }

    mpz_clears(e, h, NULL);
}

/*
 * Key Pair Generation Using Extra Random Bits
 * Input:
 *      1. p,q,g    The subset of the domain parameters
 * Output:
 *      1. x, y     The generated private (x) and public (y) keys.
 */
static void generate_keys(mpz_t x, mpz_t y, mpz_t p, mpz_t q, mpz_t g, gmp_randstate_t randstate) {
    mpz_t seed, t;

    mpz_inits(seed, t, NULL);

    // get random bits
    mpz_rrandomb(seed, randstate, N_BITLEN + 64);
    // t = q - 1
    mpz_set(t, q);
    mpz_sub_ui(t, t, 1);
    // x = (seed mod (q-1)) + 1
    mpz_mod(x, seed, t);
    mpz_add_ui(x, x, 1);

    // y = g^x mod p
    mpz_powm(y, g, x, p);

    mpz_clears(seed, t, NULL);
}

/*
 * Per-Message Secret Number Generation Using Extra Random Bits
 * Input:
 *      1. q,g      DSA domain parameters
 * Output:
 *      1. k,nk     The per-message secret number k and its mod q inverse.
 */
static void generate_K(mpz_t k, mpz_t nk, mpz_t q, mpz_t g, gmp_randstate_t randstate) {
    mpz_t seed, t;

    mpz_inits(seed, t, NULL);

    // get random bits
    mpz_rrandomb(seed, randstate, N_BITLEN + 64);
    // t = q - 1
    mpz_set(t, q);
    mpz_sub_ui(t, t, 1);
    // k = (seed mod (q-1)) + 1
    mpz_mod(k, seed, t);
    mpz_add_ui(k, k, 1);

    // nk = k^-1 mod q
    mpz_gcdext(t, nk, seed, k, q);

    mpz_clears(seed, t, NULL);
}

static void generate_signature(mpz_t r, mpz_t s, mpz_t p, mpz_t q, mpz_t g, mpz_t x,
                               gmp_randstate_t randstate, const char *msg, size_t len) {
    mpz_t k, nk, hash;

    mpz_inits(k, nk, hash, NULL);

    while (1) {
        generate_K(k, nk, q, g, randstate);
        // r = (g^k mod p) mod q
        mpz_powm(r, g, k, p);
        mpz_mod(r, r, q);

        if (!mpz_sgn(r))
            continue;

        get_hash(hash, msg, len);
        // s = ((x * r + hash) * nk) mod q
        mpz_mul(s, x, r);
        mpz_add(s, s, hash);
        mpz_mul(s, s, nk);
        mpz_mod(s, s, q);

        if (!mpz_sgn(s))
            continue;

        break;
    }

    mpz_clears(k, nk, hash, NULL);
}

static int validate_signature(mpz_t r, mpz_t s, mpz_t p, mpz_t q, mpz_t g, mpz_t y, const char *msg,
                              size_t len) {
    mpz_t w, u1, u2, v, hash, gu1, yu2;
    int res = 0;

    mpz_inits(w, u1, u2, v, hash, gu1, yu2, NULL);

    // w = s^-1 mod q
    mpz_gcdext(u1, w, u2, s, q);
    get_hash(hash, msg, len);
    // u1 = (hash * w) mod q
    mpz_mul(u1, hash, w);
    mpz_mod(u1, u1, q);
    // u2 = (r * w) mod q
    mpz_mul(u2, r, w);
    mpz_mod(u2, u2, q);
    // gu1 = g^u1 mod p
    mpz_powm(gu1, g, u1, p);
    // yu2 = y^u2 mod p
    mpz_powm(yu2, y, u2, p);
    // v = ((gu1 * yu2) mod p) mod q
    mpz_mul(v, gu1, yu2);
    mpz_mod(v, v, p);
    mpz_mod(v, v, q);

    res = !mpz_cmp(v, r);

    mpz_clears(w, u1, u2, v, hash, gu1, yu2, NULL);

    return res;
}
